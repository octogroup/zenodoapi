// Zenodo's API documentation may be found at http://developers.zenodo.org/
// Import required libraries.
import { BIO } from '@octogroup/fairy-land';
import * as request from 'request';
import { createReadStream } from 'fs';

//
// Zenodo Types.
//
// Depositions
interface Deposition {
    created: string, // timestamp in ISO8601 format.
    files: DepositionFile[],
    id: number,
    metadata: DepositionMetadata,
    modified: string, // Timestamp in ISO8601 format.
    owner: number, // User identifier of the owner of the deposition.
    state: 'inprogress' | 'done' | 'error',
    submitted: boolean, // TRUE if published.
    title: string // Title of deposition (automatically set from metadata). Defaults to empty string.
    doi?: string, // Only for published resources. Will mint a new one automatically unless provided with one from the start. Only present for published depositions.
    doi_url?: string, // Persistent link to your published deposition. This field is only present for published depositions.
    record_id?: number, // Record identifier. Only present for published depositions.
    record_url?: string, // URL to public version of record for this deposition. Only present for published depositions.
}

interface DepositionFile {
    id: string,
    filename: string,
    filesize: number,
    checksum: string,
}

interface DepositionMetadata {
    upload_type: 'publication' | 'poster' | 'presentation' | 'dataset' | 'image' | 'video' | 'software' | 'lesson' | 'other',
    publication_date: string, // Date of publication in ISO8601 format (YYYY-MM-DD). Defaults to current date.
    title: string,
    creators: Creator[],
    description: string, // Abstract or description for deposition. Following HTML tags are allowed: a, p, br, blockquote, strong, b, u, i, em, ul, ol, li, sub, sup, div, strike.
    access_right: 'open' | 'embargoed' | 'restricted' | 'closed',
    publication_type?: 'book' | 'selection' | 'conferencepaper' | 'article' | 'patent' | 'preprint' | 'report' | 'deliverable' | 'milestone' | 'proposal' | 'softwaredocumentation' | 'thesis' | 'technicalnote' | 'workingpaper' | 'other',
    image_type?: 'figure' | 'plot' | 'drawing' | 'diagram' | 'photo' | 'other',
    license?: LicenseChoice, // Required if access_right is "open" or "embargoed".
    embargo_date?: Date, // Requiured if access_right is "embargoed"
    access_conditions?: string, // Required if access_right is "restricted".
    doi?: string,
    prereserve_doi?: boolean, // Set to TRUE, to reserve a Digital Object Identifier (DOI).
    keywords?: string[],
    notes?: string, // No HTML allowed.
    related_identifiers?: RelatedIdentifier[],
    contributors?: Contributor[], // The contributors of the deposition (e.g. editors, data curators, etc.).
    references?: string[],
    communities?: Community[],
    grants?: Grant[],
    journal_title?: string,
    journal_volume?: string,
    journal_issue?: string,
    journal_pages?: string,
    conference_title?: string,
    conference_acronym?: string,
    conference_dates?: string,
    conference_place?: string,
    conference_url?: string,
    conference_session?: string,
    conference_session_part?: string,
    imprint_publisher?: string,
    imprint_isbn?: string,
    imprint_place?: string,
    partof_title?: string,
    partof_pages?: string,
    thesis_supervisors?: Creator[],
    subjects?: Subject[],
    version?: string,
    language?: string // Specify the main language of the record as ISO 639-2 or 639-3 code (e.g. 'eng'), https://www.loc.gov/standards/iso639-2/php/code_list.php
}

interface Creator {
    name: string, // Format Family name, Given names
    affiliation?: string,
    orcid?: string, // ORCID identifier of creator
    gnd?: string // GND identifier of creator
}

// Descriptions at https://licenses.opendefinition.org/
// Retrieve the current list via GET to /api/licenses
type LicenseChoice = 'cc-by-nc-4.0' | 'cc-by' | 'cc-zero';

interface RelatedIdentifier {
    identifier: string,
    relation: 'isCitedBy' | 'cites' | 'isSupplementTo' | 'isSupplementedBy' | 'isNewVersionOf' | 'isPreviousVersionOf' | 'isPartOf' | 'hasPart' | 'compiles' | 'isCompiledBy' | 'isIdenticalTo' | 'isAlternateIdentifier',
}

interface Contributor extends Creator {
    type: 'ContactPerson' | 'DataCollector' | 'DataCurator' | 'DataManager' | 'Distributor' | 'Editor' | 'Funder' | 'HostingInstitution' | 'Producer' | 'ProjectLeader' | 'ProjectManager' | 'ProjectMember' | 'RegistrationAgency' | 'RegistrationAuthority' | 'RelatedPerson' | 'Researcher' | 'ResearchGroup' | 'RightsHolder' | 'Supervisor' | 'Sponsor' | 'WorkPackageLeader' | 'Other'
}

interface Community {
    identifier: string
}

interface Grant {
    id: string
}

// Specify subjects from a taxonomy or controlled vocabulary.
// Each term must be uniquely identified (e.g. a URL). For free-form text, use the keywords field.
interface Subject {
    term: string, // Term from taxonomy or controlled vocabulary.
    identifier: string, // Unique identifier for term.
    scheme: string // Persistent identifier scheme for id (automatically detected).
}

export { Deposition, DepositionFile, DepositionMetadata, Creator, LicenseChoice, RelatedIdentifier, Contributor, Community, Grant, Subject };

// Licenses
interface License {
    created: string, // Timestamp in ISO8601 format.
    updated: string, // Timestamp in ISO8601 format.
    metadata: LicenseMetadata
}

interface LicenseMetadata {
    id: string,
    title: string,
    url: string
}

export { License, LicenseMetadata };

//
// Responses.
//
// Error responses.
interface ErrorResponse {
    message: string,
    status: number,
    errors?: ErrorObject[]
}

interface ErrorObject {
    code: number,
    message: string,
    field: string
}

// Deposition responses.
interface ListDepositionsResponse {
    code: string, // 200 OK
    body: Deposition[]
}

interface CreateDepositionResponse {
    code: string, // 201 Created
    body: Deposition
}

interface SortDepositionFilesResponse {
    code: string, // 200 OK
    body: DepositionFile[]
}

type RetrieveDepositionResponse = CreateDepositionResponse; // 200 OK
type UpdateDepositionResponse = CreateDepositionResponse; // 200 OK
type PublishDepositionRepsonse = CreateDepositionResponse; // 202 Accepted
type EditDepositionResponse = CreateDepositionResponse; // 201 Created
type DiscardDepositionResponse = CreateDepositionResponse; // 201 Created
type NewVersionDepositionResponse = CreateDepositionResponse; // 201 Created

// Only unpublished depositions may be deleted.
interface DeleteDepositionResponse {
    code: string, // 201 Created
    body: '' // Empty body.
}

// Paginated response body.
interface PaginatedResponseBody<A> {
    aggregations: {},
    hits: {
        hits: A,
        total: number
    },
    links: {
        self: string,
        next?: string
    }
}

// License responses.
interface ListLicenseResponse {
    code: string,
    body: PaginatedResponseBody<ListLicenseResponseHit[]>
}

interface ListLicenseResponseHit {
    created: string,
    links: {
        self: string
    },
    metadata: {
        '$schema': string,
        domain_content: boolean,
        doain_data: boolean,
        domain_software: boolean,
        family: string,
        id: string,
        maintainer: string,
        od_conformace: string,
        osd_conformance: string,
        status: string,
        suggest: {
            input: string[],
            output: string,
            payload: {
                id: string
            }
        },
        title: string,
        url: string
    },
    updated: string
}

interface RetrieveLicenseResponse {
    code: string,
    body: License
}

export { ErrorResponse, ErrorObject, ListDepositionsResponse, CreateDepositionResponse, SortDepositionFilesResponse, RetrieveDepositionResponse, UpdateDepositionResponse, PublishDepositionRepsonse, EditDepositionResponse, DiscardDepositionResponse, NewVersionDepositionResponse, DeleteDepositionResponse, ListLicenseResponse, ListLicenseResponseHit, RetrieveLicenseResponse, PaginatedResponseBody };

//
// List Zenodo's endpoints.
//
const base = 'https://zenodo.org/api/';
const testBase = 'https://sandbox.zenodo.org/api/';
const depositionsEndpoint = testBase + 'deposit/depositions';
const licensesEndpoint = testBase + 'licenses';

export { depositionsEndpoint, licensesEndpoint };

//
// Requests
//
type Response = request.Response;

interface ResponseWithBody<A> {
    httpResponse: Response,
    body?: A
}

interface BasicRequest {
    method: 'get' | 'put' | 'post' | 'delete',
    endpoint: string,
    token: string,
    parameters?: {
        [x: string]: string
    },
    options?: request.CoreOptions
}

export { Response, ResponseWithBody, BasicRequest };

// Request headers will (almost) always be the same.
const jsonHeaders = {
    headers: {
        'Content-Type': 'application/json'
    }
}

// We need to authenticate with a parameter.
const authenticateEndpoint = (request: BasicRequest) => {
    if (request.parameters) {
        // Map over the parameters to make a joined string.
        const reduceKV = <K extends keyof any[] | string, V, W>(reducer: (acc: W) => (kv: { key: K, value: V }) => W) =>
            (init: W) =>
                (rec: Record<K, V>): W => {
                    let acc = init
                    Object.keys(rec).forEach(key => {
                        acc = reducer(acc)({ key: key as K, value: rec[key as K] })
                    });
                    return acc
                }
        const stringifyParams = (accumulator: { iteration: number, string: string }) => (kv: { key: string, value: string }) => {
            if (accumulator.iteration === 0) {
                return {
                    iteration: accumulator.iteration++,
                    string: '?' + kv.key + '=' + kv.value
                }
            } else {
                return {
                    iteration: accumulator.iteration++,
                    string: '&' + kv.key + '=' + kv.value
                }
            }
        }
        const parameterString = reduceKV(stringifyParams)({ iteration: 0, string: '' })(request.parameters);
        return request.endpoint + parameterString.string + '&access_token=' + request.token
    } else {
        return request.endpoint + '?access_token=' + request.token;
    }
};

// Make all our requests.
const req = (r: BasicRequest) => BIO.of<any, { next: BasicRequest | null, result: ResponseWithBody<any>}>(alt => {
    const meth = r.method === 'get' ? request.get :
        r.method === 'post' ? request.post :
            r.method === 'put' ? request.put :
                r.method === 'delete' ? request.delete :
                    request.get;
    meth(authenticateEndpoint(r), r.options, (err, httpResponse, body) => {
        if (err) {
            alt.onError(err)
        }
        else if (body) {
            // Check if the results are paginated.
            const parsedBody = JSON.parse(body);
            if (parsedBody && parsedBody.links && parsedBody.links.next) {
                // Iterate through paginated results, i.e. listing Depositions and Licenses.
                if (parsedBody.links.next) {
                    const nextPageRequest: BasicRequest = {
                        method: r.method,
                        endpoint: parsedBody.links.next,
                        token: r.token,
                        options: r.options
                    }
                    // If we don't return, we keep calling the second page of results forever.
                    alt.onResult({
                        next: nextPageRequest,
                        result: { httpResponse, body }
                    })
                }
            } else {
                alt.onResult({ next: null, result: { httpResponse, body }})
            } 
        }
        else {
            alt.onResult({ next: null, result: { httpResponse, body: null }})
        }
    })
})

// Unfold a paginated request.
const unfoldRequest = (r: BasicRequest) =>
    BIO.unfold(req)(r)

// POST request to upload a file.
const postFileRequest = (token: string) => (endPoint: string) => <A>(fileMetadata: FileMetadata) =>
    BIO.of<any, ResponseWithBody<A>>(alt => {
        const formData = {
            name: fileMetadata.name,
            file: createReadStream(fileMetadata.location)
        }
        const options: request.CoreOptions = {
            formData: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
        const request: BasicRequest = {
            method: 'post',
            endpoint: endPoint,
            token: token,
            options: options
        }
        return req(request)
    });

export { req, postFileRequest };

//
// Depositions https://developers.zenodo.org/?python#depositions
//
interface ListDepositionsQueryArgs {
    'q'?: string,
    'status'?: string,
    'sort'?: string,
    'page'?: number,
    'size'?: number
}

interface Depositions {
    readonly list: (token: string) => (queryArgs: ListDepositionsQueryArgs) => BIO<any, ResponseWithBody<ListDepositionsResponse>>,
    readonly create: (token: string) => (metadata: DepositionMetadataObject) => BIO<any, ResponseWithBody<CreateDepositionResponse>>,
    readonly retrieve: (token: string) => (depositionID: string) => BIO<any, ResponseWithBody<RetrieveDepositionResponse>>,
    readonly update: (token: string) => (depositionID: string) => (depositionMetadata: DepositionMetadataObject) => BIO<any, ResponseWithBody<UpdateDepositionResponse>>,
    readonly deleteUnpublished: (token: string) => (depositionID: string) => BIO<any, ResponseWithBody<DeleteDepositionResponse>>,
}

interface DepositionMetadataObject {
    metadata: DepositionMetadata | {}
}

const depositions: Depositions = {
    // List Depositions.
    list: (token: string) => (queryArgs: ListDepositionsQueryArgs) => {
        // Attach query arguments as parameters, if needed
        if (Object.keys(queryArgs).length > 0) {
            return unfoldRequest({
                method: 'get',
                endpoint: depositionsEndpoint,
                token: token,
                parameters: queryArgs as { [key: string]: string } 
            });
        } else {
            return unfoldRequest({
                method: 'get',
                endpoint: depositionsEndpoint,
                token: token,
            });
        }
    },
    // Create a new Deposition.
    create: (token: string) => (metadata: DepositionMetadataObject) => unfoldRequest({
        method: 'post',
        endpoint: depositionsEndpoint,
        token: token,
        options: {
            body: JSON.stringify(metadata),
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }),
    // Retrieve a Deposition.
    retrieve: (token: string) => (depositionID: string) => unfoldRequest({
        method: 'get',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token,
    }),
    // Update a Deposition.
    update: (token: string) => (depositionID: string) => (depositionMetadata: DepositionMetadataObject) => unfoldRequest({
        method: 'put',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token,
        options: {
            body: JSON.stringify(depositionMetadata),
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }),
    // Delete an unpublished Deposition.
    deleteUnpublished: (token: string) => (depositionID: string) => unfoldRequest({
        method: 'delete',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token
    })
}

export { ListDepositionsQueryArgs, DepositionMetadataObject, Depositions, depositions };

//
// Deposition Actions http://developers.zenodo.org/?shell#deposition-actions
//
// List our Deposition Postfixes.
const publishDepositionPostfix = '/actions/publish';
const editDepositionPostfix = '/actions/edit';
const discardEditsDepositionPostfix = '/actions/discard';
const newVersionDepositionPostfix = '/actions/newversion';

// Helper function to make Deposition Actions.
const depositionAction = (postfix: string) => (token: string) => (id: string) => {
    const actionEndpoint = depositionsEndpoint + '/' + id + postfix;
    return unfoldRequest({
        method: 'post',
        endpoint: actionEndpoint,
        token: token,
        options: jsonHeaders
    })
}

interface DepositionActions {
    readonly publish: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<PublishDepositionRepsonse>>;
    readonly edit: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<EditDepositionResponse>>;
    readonly discardEdits: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<DiscardDepositionResponse>>;
    readonly newVersion: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<NewVersionDepositionResponse>>;
}

// Implement Deposition Actions.
const depositionActions: DepositionActions = {
    publish: depositionAction(publishDepositionPostfix),
    edit: depositionAction(editDepositionPostfix),
    discardEdits: depositionAction(discardEditsDepositionPostfix),
    newVersion: depositionAction(newVersionDepositionPostfix),
}

export { DepositionActions, depositionActions };

//
// Deposition File Actions http://developers.zenodo.org/?shell#deposition-files
//
const depositionFilesPostfix = '/files';

interface FileMetadata {
    name: string,
    location: string
}

interface FileID {
    id: string
}

interface DepositionFileActions {
    readonly list: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<ListDepositionsResponse>>;
    readonly create: (token: string) => (fileMetadata: FileMetadata) => (depositionMetadata: DepositionMetadata) => BIO<any, ResponseWithBody<CreateDepositionResponse>>;
    readonly sort: (token: string) => (id: string) => (sortedFiles: FileID[]) => BIO<ErrorResponse, ResponseWithBody<SortDepositionFilesResponse>>;
    readonly retrieve: (token: string) => (depositionID: string) => (fileID: string) => BIO<ErrorResponse, ResponseWithBody<RetrieveDepositionResponse>>;
    readonly rename: (token: string) => (depositionID: string) => (fileID: string) => (fileName: string) => BIO<ErrorResponse, ResponseWithBody<UpdateDepositionResponse>>;
    readonly deleteUnpublished: (token: string) => (depositionID: string) => (fileID: string) => BIO<ErrorResponse, ResponseWithBody<DeleteDepositionResponse>>;
}

// Implement Deposition File Actions.
const depositionFileActions: DepositionFileActions = {
    // The ID for listingFiles is the same ID returned from createDepositionFile, NOT the fileID.
    list: (token: string) => (id: string) => {
        const actionEndpoint = depositionsEndpoint + '/' + id + depositionFilesPostfix;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    },
    create: (token: string) => (fileMetadata: FileMetadata) => (depositionMetadata: DepositionMetadata) => {
        return BIO.go
            // First we need to POST the Deposition Metadata to get a Deposition ID.
            .bind('idResponse')(() => unfoldRequest({
                method: 'post',
                endpoint: depositionsEndpoint,
                token: token,
                options: {
                    body: JSON.stringify({ metadata: depositionMetadata }),
                    headers: { 'Content-Type': 'application/json' }
                }
            }))
            // Parse out the Deposition ID.
            .bind('id')(({ idResponse }) => {
                if (idResponse.body) {
                    const parsedBody = JSON.parse(idResponse.body);
                    if (parsedBody && parsedBody.id) {
                        return BIO.rpure(parsedBody.id)
                    } else {
                        return BIO.epure({ message: 'Body could not be parsed for Deposition ID.', body: idResponse.body })
                    }
                } else {
                    return BIO.epure({ message: 'Body not returned.', body: idResponse.body })
                }
            }
            )
            // Now that we have the Deposition ID, we need to mint an endpint with it.
            .map('uploadEndpoint')(({ id }) => depositionsEndpoint + '/' + id + depositionFilesPostfix)
            // Upload the file by POSTing it to the proper endpoint.
            .bind('uploadResponse')(({ uploadEndpoint }) =>
                postFileRequest(token)(uploadEndpoint)(fileMetadata))
            .pick('uploadResponse')
    },
    // Sort files. By default, the first one is shown as the preview.
    sort: (token: string) => (id: string) => (sortedFiles: FileID[]) => {
        const actionEndpoint = depositionsEndpoint + '/' + id + depositionFilesPostfix;
        return unfoldRequest({
            method: 'put',
            endpoint: actionEndpoint,
            token: token,
            options: {
                body: JSON.stringify(sortedFiles),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });
    },
    // Retrieve files from a given Deposition.
    retrieve: (token: string) => (depositionID: string) => (fileID: string) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    },
    // Update a Deposition File. Currently the only use is renaming an already uploaded file.
    // If you one to replace the actual file, please delete the file and upload a new file.
    rename: (token: string) => (depositionID: string) => (fileID: string) => (fileName: string) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        const fileOptions = {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filename: fileName
            })
        }
        return unfoldRequest({
            method: 'put',
            endpoint: actionEndpoint,
            token: token,
            options: fileOptions
        });
    },
    // Delete an existing deposition file resource. Only deposition files for unpublished depositions may be deleted.
    // Returns an empty body if successful.
    deleteUnpublished: (token: string) => (depositionID: string) => (fileID: string) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        return unfoldRequest({
            method: 'delete',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    }
};

export { FileMetadata, FileID, DepositionFileActions, depositionFileActions };

//
// License Actions.
//
interface LicenseActions {
    readonly list: <A>(token: string) => BIO<ErrorResponse, ResponseWithBody<ListLicenseResponse>>;
    readonly retrieve: (token: string) => (license: string) => BIO<ErrorResponse, ResponseWithBody<RetrieveLicenseResponse>>;
}

const licenseActions: LicenseActions = {
    // We still need to add pagination to listLicenses.
    list: (token: string) => unfoldRequest({
        method: 'get',
        endpoint: licensesEndpoint,
        token: token,
        options: jsonHeaders
    }),
    retrieve: (token: string) => (license: string) => {
        const actionEndpoint = licensesEndpoint + '/' + license;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    }
}

export { LicenseActions, licenseActions };
