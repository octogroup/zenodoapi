"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Zenodo's API documentation may be found at http://developers.zenodo.org/
// Import required libraries.
const fairy_land_1 = require("@octogroup/fairy-land");
const request = require("request");
const fs_1 = require("fs");
//
// List Zenodo's endpoints.
//
const base = 'https://zenodo.org/api/';
const testBase = 'https://sandbox.zenodo.org/api/';
const depositionsEndpoint = testBase + 'deposit/depositions';
exports.depositionsEndpoint = depositionsEndpoint;
const licensesEndpoint = testBase + 'licenses';
exports.licensesEndpoint = licensesEndpoint;
// Request headers will (almost) always be the same.
const jsonHeaders = {
    headers: {
        'Content-Type': 'application/json'
    }
};
// We need to authenticate with a parameter.
const authenticateEndpoint = (request) => {
    if (request.parameters) {
        // Map over the parameters to make a joined string.
        const reduceKV = (reducer) => (init) => (rec) => {
            let acc = init;
            Object.keys(rec).forEach(key => {
                acc = reducer(acc)({ key: key, value: rec[key] });
            });
            return acc;
        };
        const stringifyParams = (accumulator) => (kv) => {
            if (accumulator.iteration === 0) {
                return {
                    iteration: accumulator.iteration++,
                    string: '?' + kv.key + '=' + kv.value
                };
            }
            else {
                return {
                    iteration: accumulator.iteration++,
                    string: '&' + kv.key + '=' + kv.value
                };
            }
        };
        const parameterString = reduceKV(stringifyParams)({ iteration: 0, string: '' })(request.parameters);
        return request.endpoint + parameterString.string + '&access_token=' + request.token;
    }
    else {
        return request.endpoint + '?access_token=' + request.token;
    }
};
// Make all our requests.
const req = (r) => fairy_land_1.BIO.of(alt => {
    const meth = r.method === 'get' ? request.get :
        r.method === 'post' ? request.post :
            r.method === 'put' ? request.put :
                r.method === 'delete' ? request.delete :
                    request.get;
    meth(authenticateEndpoint(r), r.options, (err, httpResponse, body) => {
        if (err) {
            alt.onError(err);
        }
        else if (body) {
            // Check if the results are paginated.
            const parsedBody = JSON.parse(body);
            if (parsedBody && parsedBody.links && parsedBody.links.next) {
                // Iterate through paginated results, i.e. listing Depositions and Licenses.
                if (parsedBody.links.next) {
                    const nextPageRequest = {
                        method: r.method,
                        endpoint: parsedBody.links.next,
                        token: r.token,
                        options: r.options
                    };
                    // If we don't return, we keep calling the second page of results forever.
                    alt.onResult({
                        next: nextPageRequest,
                        result: { httpResponse, body }
                    });
                }
            }
            else {
                alt.onResult({ next: null, result: { httpResponse, body } });
            }
        }
        else {
            alt.onResult({ next: null, result: { httpResponse, body: null } });
        }
    });
});
exports.req = req;
// Unfold a paginated request.
const unfoldRequest = (r) => fairy_land_1.BIO.unfold(req)(r);
// POST request to upload a file.
const postFileRequest = (token) => (endPoint) => (fileMetadata) => fairy_land_1.BIO.of(alt => {
    const formData = {
        name: fileMetadata.name,
        file: fs_1.createReadStream(fileMetadata.location)
    };
    const options = {
        formData: formData,
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    };
    const request = {
        method: 'post',
        endpoint: endPoint,
        token: token,
        options: options
    };
    return req(request);
});
exports.postFileRequest = postFileRequest;
const depositions = {
    // List Depositions.
    list: (token) => (queryArgs) => {
        // Attach query arguments as parameters, if needed
        if (Object.keys(queryArgs).length > 0) {
            return unfoldRequest({
                method: 'get',
                endpoint: depositionsEndpoint,
                token: token,
                parameters: queryArgs
            });
        }
        else {
            return unfoldRequest({
                method: 'get',
                endpoint: depositionsEndpoint,
                token: token,
            });
        }
    },
    // Create a new Deposition.
    create: (token) => (metadata) => unfoldRequest({
        method: 'post',
        endpoint: depositionsEndpoint,
        token: token,
        options: {
            body: JSON.stringify(metadata),
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }),
    // Retrieve a Deposition.
    retrieve: (token) => (depositionID) => unfoldRequest({
        method: 'get',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token,
    }),
    // Update a Deposition.
    update: (token) => (depositionID) => (depositionMetadata) => unfoldRequest({
        method: 'put',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token,
        options: {
            body: JSON.stringify(depositionMetadata),
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }),
    // Delete an unpublished Deposition.
    deleteUnpublished: (token) => (depositionID) => unfoldRequest({
        method: 'delete',
        endpoint: depositionsEndpoint + '/' + depositionID,
        token: token
    })
};
exports.depositions = depositions;
//
// Deposition Actions http://developers.zenodo.org/?shell#deposition-actions
//
// List our Deposition Postfixes.
const publishDepositionPostfix = '/actions/publish';
const editDepositionPostfix = '/actions/edit';
const discardEditsDepositionPostfix = '/actions/discard';
const newVersionDepositionPostfix = '/actions/newversion';
// Helper function to make Deposition Actions.
const depositionAction = (postfix) => (token) => (id) => {
    const actionEndpoint = depositionsEndpoint + '/' + id + postfix;
    return unfoldRequest({
        method: 'post',
        endpoint: actionEndpoint,
        token: token,
        options: jsonHeaders
    });
};
// Implement Deposition Actions.
const depositionActions = {
    publish: depositionAction(publishDepositionPostfix),
    edit: depositionAction(editDepositionPostfix),
    discardEdits: depositionAction(discardEditsDepositionPostfix),
    newVersion: depositionAction(newVersionDepositionPostfix),
};
exports.depositionActions = depositionActions;
//
// Deposition File Actions http://developers.zenodo.org/?shell#deposition-files
//
const depositionFilesPostfix = '/files';
// Implement Deposition File Actions.
const depositionFileActions = {
    // The ID for listingFiles is the same ID returned from createDepositionFile, NOT the fileID.
    list: (token) => (id) => {
        const actionEndpoint = depositionsEndpoint + '/' + id + depositionFilesPostfix;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    },
    create: (token) => (fileMetadata) => (depositionMetadata) => {
        return fairy_land_1.BIO.go
            // First we need to POST the Deposition Metadata to get a Deposition ID.
            .bind('idResponse')(() => unfoldRequest({
            method: 'post',
            endpoint: depositionsEndpoint,
            token: token,
            options: {
                body: JSON.stringify({ metadata: depositionMetadata }),
                headers: { 'Content-Type': 'application/json' }
            }
        }))
            // Parse out the Deposition ID.
            .bind('id')(({ idResponse }) => {
            if (idResponse.body) {
                const parsedBody = JSON.parse(idResponse.body);
                if (parsedBody && parsedBody.id) {
                    return fairy_land_1.BIO.rpure(parsedBody.id);
                }
                else {
                    return fairy_land_1.BIO.epure({ message: 'Body could not be parsed for Deposition ID.', body: idResponse.body });
                }
            }
            else {
                return fairy_land_1.BIO.epure({ message: 'Body not returned.', body: idResponse.body });
            }
        })
            // Now that we have the Deposition ID, we need to mint an endpint with it.
            .map('uploadEndpoint')(({ id }) => depositionsEndpoint + '/' + id + depositionFilesPostfix)
            // Upload the file by POSTing it to the proper endpoint.
            .bind('uploadResponse')(({ uploadEndpoint }) => postFileRequest(token)(uploadEndpoint)(fileMetadata))
            .pick('uploadResponse');
    },
    // Sort files. By default, the first one is shown as the preview.
    sort: (token) => (id) => (sortedFiles) => {
        const actionEndpoint = depositionsEndpoint + '/' + id + depositionFilesPostfix;
        return unfoldRequest({
            method: 'put',
            endpoint: actionEndpoint,
            token: token,
            options: {
                body: JSON.stringify(sortedFiles),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });
    },
    // Retrieve files from a given Deposition.
    retrieve: (token) => (depositionID) => (fileID) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    },
    // Update a Deposition File. Currently the only use is renaming an already uploaded file.
    // If you one to replace the actual file, please delete the file and upload a new file.
    rename: (token) => (depositionID) => (fileID) => (fileName) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        const fileOptions = {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filename: fileName
            })
        };
        return unfoldRequest({
            method: 'put',
            endpoint: actionEndpoint,
            token: token,
            options: fileOptions
        });
    },
    // Delete an existing deposition file resource. Only deposition files for unpublished depositions may be deleted.
    // Returns an empty body if successful.
    deleteUnpublished: (token) => (depositionID) => (fileID) => {
        const actionEndpoint = depositionsEndpoint + '/' + depositionID + depositionFilesPostfix + '/' + fileID;
        return unfoldRequest({
            method: 'delete',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    }
};
exports.depositionFileActions = depositionFileActions;
const licenseActions = {
    // We still need to add pagination to listLicenses.
    list: (token) => unfoldRequest({
        method: 'get',
        endpoint: licensesEndpoint,
        token: token,
        options: jsonHeaders
    }),
    retrieve: (token) => (license) => {
        const actionEndpoint = licensesEndpoint + '/' + license;
        return unfoldRequest({
            method: 'get',
            endpoint: actionEndpoint,
            token: token,
            options: jsonHeaders
        });
    }
};
exports.licenseActions = licenseActions;
//# sourceMappingURL=index.js.map