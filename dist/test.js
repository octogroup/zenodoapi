"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// Test!
//
const index_1 = require("./index");
const testToken = 'jTbkkmukWTdvKltzsHeF6be0qrYR3ME8y4Hcli0MIW9vtioEYFvymjBchGEZ';
const liveToken = 'VfxC3Omwozxlf5jm6rnUOVXs0bXcW6WkGDwQ5AS6xAtCK6GJK6j1GrVj6D3a';
const uploadID = '276603';
const sortedFiles = {
    id: '21f929b7-5aab-49da-8c4d-a19a62a47251'
};
const depoMeta = {
    upload_type: 'publication',
    publication_type: 'preprint',
    publication_date: '2019-05-01',
    title: 'Nick test paper',
    creators: [{
            name: 'Nick Wehner',
            affiliation: 'OCTO'
        }],
    description: 'This is a test',
    access_right: 'open'
};
// Test pagination. Needs work.
index_1.licenseActions.list(testToken).unsafeResolve({
    onError: err => console.log(err),
    onResult: result => console.log(result) //localLog(2)
});
// Works!
// Test discarding edits.
// const testDiscardEditing = depositionActions.discardEditsDeposition(testToken)(uploadID).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Test editing.
// const testEditing = depositionActions.editDeposition(testToken)(uploadID).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Test new version.
// const testNewVersion = depositionActions.newVersionDeposition(testToken)(uploadID).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Test publishing our data.
// const testPublish = depositionActions.publishDeposition(testToken)(uploadID).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// This returns 404s (for IDs that definitely exist) in the sandbox.
// Delete our testing depo file.
// const testDeleteDepoFile = depositionFileActions.deleteUnpublishedDepositonFile(liveToken)(uploadID)(sortedFiles.id).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Rename Deposition File.
// const testRenameDepoFile = depositionFileActions.renameDepositionFile(token)(uploadID)(sortedFiles.id)("JuniperTest.pdf").unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Retrieve Deposition Files
// const testRetreiveDepoFiles = depositionFileActions.retrieveDepositionFiles(token)(uploadID)(sortedFiles.id).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Sort Deposition Files.
// const testSortDepFiles = depositionFileActions.sortDepositionFiles(token)(uploadID)([sortedFiles]).unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// List Deposition Files.
// const testListDepositionFiles = depositionFileActions.listDepositionFiles(token)(uploadID);
// testListDepositionFiles.unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// })
// Works!
// Test creating a Deposition File.
// const fileData: FileMetadata = {
//     name: 'ActionTest.pdf',
//     location: '././files/Ep38ShowNotes.pdf'
// }
// const testUploadFile = depositionFileActions.createDepositionFile(testToken)(fileData)(depoMeta);
// testUploadFile.unsafeResolve({
//     onError: localLog(2),
//     onResult: localLog(2)
// });
// Works!
// const depositions = getRequest(token)(depositionsEndpoint).unsafeResolve({
//     onResult: a => localLog(a),
//     onError: e => localLog(e)
// });
//# sourceMappingURL=test.js.map