import { BIO } from '@octogroup/fairy-land';
import * as request from 'request';
interface Deposition {
    created: string;
    files: DepositionFile[];
    id: number;
    metadata: DepositionMetadata;
    modified: string;
    owner: number;
    state: 'inprogress' | 'done' | 'error';
    submitted: boolean;
    title: string;
    doi?: string;
    doi_url?: string;
    record_id?: number;
    record_url?: string;
}
interface DepositionFile {
    id: string;
    filename: string;
    filesize: number;
    checksum: string;
}
interface DepositionMetadata {
    upload_type: 'publication' | 'poster' | 'presentation' | 'dataset' | 'image' | 'video' | 'software' | 'lesson' | 'other';
    publication_date: string;
    title: string;
    creators: Creator[];
    description: string;
    access_right: 'open' | 'embargoed' | 'restricted' | 'closed';
    publication_type?: 'book' | 'selection' | 'conferencepaper' | 'article' | 'patent' | 'preprint' | 'report' | 'deliverable' | 'milestone' | 'proposal' | 'softwaredocumentation' | 'thesis' | 'technicalnote' | 'workingpaper' | 'other';
    image_type?: 'figure' | 'plot' | 'drawing' | 'diagram' | 'photo' | 'other';
    license?: LicenseChoice;
    embargo_date?: Date;
    access_conditions?: string;
    doi?: string;
    prereserve_doi?: boolean;
    keywords?: string[];
    notes?: string;
    related_identifiers?: RelatedIdentifier[];
    contributors?: Contributor[];
    references?: string[];
    communities?: Community[];
    grants?: Grant[];
    journal_title?: string;
    journal_volume?: string;
    journal_issue?: string;
    journal_pages?: string;
    conference_title?: string;
    conference_acronym?: string;
    conference_dates?: string;
    conference_place?: string;
    conference_url?: string;
    conference_session?: string;
    conference_session_part?: string;
    imprint_publisher?: string;
    imprint_isbn?: string;
    imprint_place?: string;
    partof_title?: string;
    partof_pages?: string;
    thesis_supervisors?: Creator[];
    subjects?: Subject[];
    version?: string;
    language?: string;
}
interface Creator {
    name: string;
    affiliation?: string;
    orcid?: string;
    gnd?: string;
}
declare type LicenseChoice = 'cc-by-nc-4.0' | 'cc-by' | 'cc-zero';
interface RelatedIdentifier {
    identifier: string;
    relation: 'isCitedBy' | 'cites' | 'isSupplementTo' | 'isSupplementedBy' | 'isNewVersionOf' | 'isPreviousVersionOf' | 'isPartOf' | 'hasPart' | 'compiles' | 'isCompiledBy' | 'isIdenticalTo' | 'isAlternateIdentifier';
}
interface Contributor extends Creator {
    type: 'ContactPerson' | 'DataCollector' | 'DataCurator' | 'DataManager' | 'Distributor' | 'Editor' | 'Funder' | 'HostingInstitution' | 'Producer' | 'ProjectLeader' | 'ProjectManager' | 'ProjectMember' | 'RegistrationAgency' | 'RegistrationAuthority' | 'RelatedPerson' | 'Researcher' | 'ResearchGroup' | 'RightsHolder' | 'Supervisor' | 'Sponsor' | 'WorkPackageLeader' | 'Other';
}
interface Community {
    identifier: string;
}
interface Grant {
    id: string;
}
interface Subject {
    term: string;
    identifier: string;
    scheme: string;
}
export { Deposition, DepositionFile, DepositionMetadata, Creator, LicenseChoice, RelatedIdentifier, Contributor, Community, Grant, Subject };
interface License {
    created: string;
    updated: string;
    metadata: LicenseMetadata;
}
interface LicenseMetadata {
    id: string;
    title: string;
    url: string;
}
export { License, LicenseMetadata };
interface ErrorResponse {
    message: string;
    status: number;
    errors?: ErrorObject[];
}
interface ErrorObject {
    code: number;
    message: string;
    field: string;
}
interface ListDepositionsResponse {
    code: string;
    body: Deposition[];
}
interface CreateDepositionResponse {
    code: string;
    body: Deposition;
}
interface SortDepositionFilesResponse {
    code: string;
    body: DepositionFile[];
}
declare type RetrieveDepositionResponse = CreateDepositionResponse;
declare type UpdateDepositionResponse = CreateDepositionResponse;
declare type PublishDepositionRepsonse = CreateDepositionResponse;
declare type EditDepositionResponse = CreateDepositionResponse;
declare type DiscardDepositionResponse = CreateDepositionResponse;
declare type NewVersionDepositionResponse = CreateDepositionResponse;
interface DeleteDepositionResponse {
    code: string;
    body: '';
}
interface PaginatedResponseBody<A> {
    aggregations: {};
    hits: {
        hits: A;
        total: number;
    };
    links: {
        self: string;
        next?: string;
    };
}
interface ListLicenseResponse {
    code: string;
    body: PaginatedResponseBody<ListLicenseResponseHit[]>;
}
interface ListLicenseResponseHit {
    created: string;
    links: {
        self: string;
    };
    metadata: {
        '$schema': string;
        domain_content: boolean;
        doain_data: boolean;
        domain_software: boolean;
        family: string;
        id: string;
        maintainer: string;
        od_conformace: string;
        osd_conformance: string;
        status: string;
        suggest: {
            input: string[];
            output: string;
            payload: {
                id: string;
            };
        };
        title: string;
        url: string;
    };
    updated: string;
}
interface RetrieveLicenseResponse {
    code: string;
    body: License;
}
export { ErrorResponse, ErrorObject, ListDepositionsResponse, CreateDepositionResponse, SortDepositionFilesResponse, RetrieveDepositionResponse, UpdateDepositionResponse, PublishDepositionRepsonse, EditDepositionResponse, DiscardDepositionResponse, NewVersionDepositionResponse, DeleteDepositionResponse, ListLicenseResponse, ListLicenseResponseHit, RetrieveLicenseResponse, PaginatedResponseBody };
declare const depositionsEndpoint: string;
declare const licensesEndpoint: string;
export { depositionsEndpoint, licensesEndpoint };
declare type Response = request.Response;
interface ResponseWithBody<A> {
    httpResponse: Response;
    body?: A;
}
interface BasicRequest {
    method: 'get' | 'put' | 'post' | 'delete';
    endpoint: string;
    token: string;
    parameters?: {
        [x: string]: string;
    };
    options?: request.CoreOptions;
}
export { Response, ResponseWithBody, BasicRequest };
declare const req: (r: BasicRequest) => BIO<any, {
    next: BasicRequest | null;
    result: ResponseWithBody<any>;
}>;
declare const postFileRequest: (token: string) => (endPoint: string) => <A>(fileMetadata: FileMetadata) => BIO<any, ResponseWithBody<A>>;
export { req, postFileRequest };
interface ListDepositionsQueryArgs {
    'q'?: string;
    'status'?: string;
    'sort'?: string;
    'page'?: number;
    'size'?: number;
}
interface Depositions {
    readonly list: (token: string) => (queryArgs: ListDepositionsQueryArgs) => BIO<any, ResponseWithBody<ListDepositionsResponse>>;
    readonly create: (token: string) => (metadata: DepositionMetadataObject) => BIO<any, ResponseWithBody<CreateDepositionResponse>>;
    readonly retrieve: (token: string) => (depositionID: string) => BIO<any, ResponseWithBody<RetrieveDepositionResponse>>;
    readonly update: (token: string) => (depositionID: string) => (depositionMetadata: DepositionMetadataObject) => BIO<any, ResponseWithBody<UpdateDepositionResponse>>;
    readonly deleteUnpublished: (token: string) => (depositionID: string) => BIO<any, ResponseWithBody<DeleteDepositionResponse>>;
}
interface DepositionMetadataObject {
    metadata: DepositionMetadata | {};
}
declare const depositions: Depositions;
export { ListDepositionsQueryArgs, DepositionMetadataObject, Depositions, depositions };
interface DepositionActions {
    readonly publish: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<PublishDepositionRepsonse>>;
    readonly edit: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<EditDepositionResponse>>;
    readonly discardEdits: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<DiscardDepositionResponse>>;
    readonly newVersion: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<NewVersionDepositionResponse>>;
}
declare const depositionActions: DepositionActions;
export { DepositionActions, depositionActions };
interface FileMetadata {
    name: string;
    location: string;
}
interface FileID {
    id: string;
}
interface DepositionFileActions {
    readonly list: (token: string) => (id: string) => BIO<ErrorResponse, ResponseWithBody<ListDepositionsResponse>>;
    readonly create: (token: string) => (fileMetadata: FileMetadata) => (depositionMetadata: DepositionMetadata) => BIO<any, ResponseWithBody<CreateDepositionResponse>>;
    readonly sort: (token: string) => (id: string) => (sortedFiles: FileID[]) => BIO<ErrorResponse, ResponseWithBody<SortDepositionFilesResponse>>;
    readonly retrieve: (token: string) => (depositionID: string) => (fileID: string) => BIO<ErrorResponse, ResponseWithBody<RetrieveDepositionResponse>>;
    readonly rename: (token: string) => (depositionID: string) => (fileID: string) => (fileName: string) => BIO<ErrorResponse, ResponseWithBody<UpdateDepositionResponse>>;
    readonly deleteUnpublished: (token: string) => (depositionID: string) => (fileID: string) => BIO<ErrorResponse, ResponseWithBody<DeleteDepositionResponse>>;
}
declare const depositionFileActions: DepositionFileActions;
export { FileMetadata, FileID, DepositionFileActions, depositionFileActions };
interface LicenseActions {
    readonly list: <A>(token: string) => BIO<ErrorResponse, ResponseWithBody<ListLicenseResponse>>;
    readonly retrieve: (token: string) => (license: string) => BIO<ErrorResponse, ResponseWithBody<RetrieveLicenseResponse>>;
}
declare const licenseActions: LicenseActions;
export { LicenseActions, licenseActions };
